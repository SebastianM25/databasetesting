import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.DriverManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class JDBC_TestExample {

    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;
    private static PreparedStatement preparedStatement;

    @BeforeClass
    public static void setUp() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sakila", "root",
                    "Sebastian17!");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void selectTest() throws Exception {
        statement = connection.createStatement();
        resultSet = statement.executeQuery("select * from actor");

        int count = 0;
        while (resultSet.next()) {
            System.out.println("ID for record #" + resultSet.getRow()
                    + " is: " + resultSet.getInt("actor_id"));
            System.out.println("FirstName for record #" + resultSet.getRow()
                    + " is: " + resultSet.getString("first_name"));
            System.out.println("LastName for record #" + resultSet.getRow()
                    + " is: " + resultSet.getString("last_name"));
            System.out.println("Update for record #" + resultSet.getRow()
                    + " is: " + resultSet.getDate("last_update"));
            count++;
        }
        resultSet.last();
        int noOfRows = resultSet.getRow();

        Assert.assertEquals(count, 200);
        Assert.assertEquals(noOfRows, 200);
    }

    @Test
    public void selectAllfromTable() {
        try {
            //ResultSet resultSet = statement.executeQuery("SELECT * FROM actor");
            statement = connection.createStatement();
            ResultSet resultSet1=statement.executeQuery("select * from actor");
            while (resultSet1.next()) {
                System.out.println("Row with number: " + resultSet1.getRow() + " had id " + resultSet1.getInt("actor_id"));
            }
            resultSet1.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void displayValueFromFirstName() {
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM actor");
            List<String> names = new ArrayList<>();
            while (resultSet.next()) {
                String name = resultSet.getString("first_name");
                names.add(name);

            }
            System.out.println("The first_name from ACTOR are:"+names);
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void useLikeOperator() {
        try {
            statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM actor WHERE last_name LIKE ?");
            preparedStatement.setString(1, "b%");
            ResultSet resultSet = preparedStatement.executeQuery();
            List<String> names = new ArrayList<>();
            while (resultSet.next()) {
                String name = resultSet.getString("last_name");
                names.add(name);
            }
            System.out.println(names);
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @AfterTest
    public void cleanUp() {
        try {
            connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
